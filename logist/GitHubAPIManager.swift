//
//  GitHubAPIManager.swift
//  logist
//
//  Created by ANDROMEDA on 7/15/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GitHubAPIManager {

    static let sharedInstance = GitHubAPIManager()
    
    func printPublicGists() -> Void {
        
        Alamofire.request(GistRouter.GetPublic())
            .responseString { response in
                
                if let receivedString = response.result.value {
                    print(receivedString)
                }
        }
    }

}