//
//  GistRouter.swift
//  logist
//
//  Created by ANDROMEDA on 7/15/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import Foundation
import Alamofire

enum GistRouter: URLRequestConvertible {
    
    static let baseURL: String = "https://api.github.com"
    
    case GetPublic()
    
    var URLRequest: NSMutableURLRequest {
        var method: Alamofire.Method {
            switch self {
            case .GetPublic:
                return .GET
            }
        }
        
        let result: (path: String, parameters: [String: AnyObject]?) = {
            switch self {
            case .GetPublic:
                return ("/gists/public", nil)
            }
        }()
        
        
        //url Request
        let URL = NSURL(string: GistRouter.baseURL)
        let URLRequest = NSMutableURLRequest(URL: URL!.URLByAppendingPathComponent(result.path))
        
        //encoding
        let encoding = Alamofire.ParameterEncoding.JSON
        let (encodedRequest, _) = encoding.encode(URLRequest, parameters: result.parameters)
        
        encodedRequest.HTTPMethod = method.rawValue
        
        return encodedRequest
    }

}